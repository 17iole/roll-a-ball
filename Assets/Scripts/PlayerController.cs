﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public float speed;
	private Rigidbody rb;
	private int count, resta;
	public Text countText;
	public Text winText;
	public GameObject PrefabParticulas;
	public GameObject foto;
	public GameObject eloi;
	public GameObject who;
	public GameObject boton;
	public GameObject winAudio;
	public Animator animator;
	void Start(){
		count = 0;
		rb = GetComponent<Rigidbody> ();
		setCountText ();
		winText.text = "";
	}

	// Update is called once per frame
	void FixedUpdate () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0, moveVertical);
		rb.AddForce (movement*speed);

	}
	public Quaternion rotacionParticulas;

	void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag("pickup")){
			Instantiate (PrefabParticulas, other.gameObject.transform.position, other.gameObject.transform.parent.rotation, other.gameObject.transform.parent);
			Debug.Log ("instanciaaa");
			other.gameObject.SetActive (false);
			other.gameObject.GetComponent<Rotator> ().letra.SetActive (true);
			count=count+1;
			gameObject.GetComponent<AudioSource>().Play();

			setCountText ();
			if(count>=17){
				winText.text = "Eloi Casamayor Esteve";
				foto.SetActive (true);
				eloi.SetActive (true);
				who.SetActive (false);
				boton.SetActive (true);
				winAudio.SetActive (true);
				animator.Play ("canvas 2");
			}
		}
	}

	void setCountText(){
		resta = 17 - count;
		countText.text =resta.ToString ();
	}
}